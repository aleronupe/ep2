Alexandre Miguel Rodrigues Nunes Pereira
16/0000840
Orientação a Objetos
EP2

Especificações:
- Feito em "Eclipse Oxygen.1"
- Java.SE_1.7
- Utilização da JFreeChart

***Para Evitar o ERRO em que não é possível encontrar o caminho calculadora/comeco, é necessário, ao abrir o projeto no Eclipse, selecionar a opção da pasta do projeto(Exemplo: EP2[Ep2 master]), clicar com o botão direito e selecionar a opção "properties". Uma vez dentro da aba "properties", clicar em "Java Build Path", selecionar a aba "Libraries" e remover a JAR "jfreechart-1.019-demo.jar-/home/alexandre/Downloads/jfree..." 

*** Manual do usuário ***
- Para começar a utilizar o programa "Aprenda QEE" é necessário executar a pasta de arquivos "ep2" no programa "Eclipse";
- Assim, o programa abrirá a Janela Primária, exibindo duas opções em JRadioButtons:
	- Fluxo de Potência Fundamental;
	- Distorção Harmônica;
- Cabe entao ao usuário selecionar alguma das duas opções e, posteriormente, clicar no botão "iniciar" para começar a simulação;
- Caso o usuário deseje sair do Programa, basta selecionar a opção de fechar a aba nesse menu (botão 'x').

		- Caso o Usuário selecione a opção "Fluxo de Potência Fundamental":
 Ao clicar no botão "Iniciar", a Janela Primária será fechada e a Janela do Fluxo de Potência Fundamental será aberta, exibindo 3 gráficos (de Forma da Onda de Tensao, Forma da Onda de Corrente e Forma da Onda de Potência), o quadro do triângulo de potências, 4 linhas com valores de resultado equivalentes a zero (Fator de Potência, Potência Reativa, Potência Ativa e Potência Aparente, respectivamente) e, por fim, 4 JFormattedTextField,  tomados em pares, com botões de "Ok" abaixo de cada Par. Cada Par de JFormattedTextField equivale à entradas: um para a amplitude e o angulo de fase da tensão e o outro para a amplitude e o ângulo de fase da corrente, indicados acima de cada JFormattedTextField.
Os valores de entrada são restritos da seguinte forma:
* Amplitude de Tensao: deve ir 0 a 220;
* Ângulo de Tensao: deve ir -180 a 180;
* Amplitude de Corrente: deve ir de 0 a 100;
* Ângulo de Corrente: deve ir de -180 a 180;
- Caso o usuário insira valores fora desses intervalos, o programa exibirá um painel informando o erro cometido e a faixa de valores válidos;
- Caso o usuário insira letras ou caracteres inválidos, o programa exibirá a mensagem "Entrada Inválida, só devem ser inseridos números!"
- Caso o usuario insira valores válidos, cada botão "Ok" ativará a construção do gráfico correspondente, enquanto o Triângulo de Potências e o gráfico da Forma da Onda de Potência serão construídos independentemente do botão pressionado.
- A qualquer momento, o usuário poderá pressionar o botão "Voltar", localizado no canto inferior direito, para retornar à Janela Primária.

		- Caso o Usuário selecione a opção "Distorção Harmônica":
Ao clicar no botão "Iniciar", a Janela Primária será fechada e a Janela da Distorção Harmônica será aberta, exibindo 2 gráficos (de Forma da Onda Fundamental e Forma da Onda Distorcida Resultante), um JScrollPane com 6 gráficos referentes às ordens harmônicas, 2 JFormattedTextField, um JSpinner, 2 JRadioButtons e, por fim, um botão "Ok" e um Botão "Voltar". Cada FormattedTextField serve de entrada para o valor do Ângulo da Onda Fundamental e Amplitude da Onda Fundamental, respectivamente e conforme indicado pelas JLabels acima de cada TextField. O JSpinner mostrado refere-se ao número de harmônicos e os JRadioButtons servem para indicar se os harmônicos serão pares ou ímpares.
Os valores de entrada são restritos da seguinte forma:
* Amplitude da Onda Fundamental: deve ir de 0 a 220;
* Ângulo da Onda Fundamental: deve ir de -180 a 180;
* Número de Harmônicos: deve ir 0 a 6;
- Caso o usuário insira valores fora desses intervalos, o programa exibirá um painel informando o erro cometido e a faixa de valores válidos;
- Caso o usuário insira letras ou caracteres inválidos, o programa exibirá a mensagem "Entrada Inválida, só devem ser inseridos números!"
- Caso o usuario insira valores válidos, ao acionar o botão "Ok" os botões "Simular" de cada gráfico referente ao número do harmônico válido será habilitado (ou seja, caso o valor inserido seja 5, todos os gráficos de 1 a 5 terão o botão "Simular" habilitado), enquanto os gráficos de Forma da Onda Fundamental e Forma da Onda Distorcida Resultante serão construídos e a JLabel correspondente à série de Fourrier resultante será escrita abaixo da Label "Série de Fourrier Amplitude-Fase".
- Assim, para cada gráfico se tem 2 JFormattedTextField (correspondente à amplitude e ao ângulo de Fase, dentro dos mesmos limites estabelecidos para a amplitde e o ângulo da Onda Fundamental), um JSpinner, correspondete à Ordem Harmônica de cada Gráfico, cujo valor válido vai de 0 a 15;
- Logo, para visualizar o gráfico de cada número de Harmônico, basta inserir os valores válidos e apertar o botão "Simular";
- A qualquer momento, o usuário poderá pressionar o botão "Voltar", localizado no canto inferior direito, para retornar à Janela Primária.




Modelagem - Diagrama de Classes em anexo na pasta "doc"
Testes Unitários - Pacote "Testes".
