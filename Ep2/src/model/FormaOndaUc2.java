package model;

import java.util.ArrayList;

public class FormaOndaUc2 {
	double amplitudeTensao;
	double anguloTensao;
	double amplitudeCorrente;
	double anguloCorrente;
	double omega = 2*3.14*60;
	
	
	
	public ArrayList<Double> getArrayList(double amplitude, double angulo){
		double i = 0.0;
		double value = 0.0;
		ArrayList<Double> result = new ArrayList<Double>();
		for(i = 0.0; i < 100; i++) {
			value = amplitude*Math.cos((omega*i)+ Math.toRadians(angulo));
			result.add(value);
		}
		return result;
	}
	
	public ArrayList<Double> getPotencia (double amplitudeTensao, double anguloTensao , double amplitudeCorrente, double anguloCorrente ){
		double i = 0.0;
		double value = 0.0;
		double valueCorr = 0.0;
		double valueTens = 0.0;
		double angulo = 0.0;
		if(anguloTensao < 0) {
			anguloTensao = 360 + anguloTensao;
		}
		if(anguloCorrente < 0) {
			anguloCorrente = 360 + anguloCorrente;
		}
		if(Math.max(anguloTensao, anguloCorrente) - Math.min(anguloTensao, anguloCorrente) > 180) {
			if(anguloTensao >= 225 && anguloCorrente <= 45) {
				angulo = 360 - anguloTensao + anguloCorrente;
			}
			else if(anguloTensao >= 225 && anguloCorrente > 45) {
				angulo = 360 - anguloTensao + anguloCorrente;
			}
			else if(anguloTensao >= 90 && anguloTensao <= 135 && anguloCorrente >= 315) {
				angulo = anguloCorrente - 180 + 180 - anguloTensao;
			}
			else if(anguloCorrente >= 225 && anguloTensao <= 45) {
				angulo = 360 - anguloCorrente + anguloTensao;
			}
			else if(anguloCorrente >= 225 && anguloTensao > 45) {
				angulo = 360 - anguloCorrente + anguloTensao;
			}
			else if(anguloCorrente >= 90 && anguloCorrente <= 135 && anguloTensao >= 315) {
				angulo = anguloTensao - 180 + 180 - anguloCorrente;
			}
		}
		else {
			angulo = Math.max(anguloTensao, anguloCorrente) - Math.min(anguloTensao, anguloCorrente);
		}
		
		ArrayList<Double> result = new ArrayList<Double>();
		for(i = 0.0; i < 100; i += 0.1) {
			
			valueCorr = amplitudeCorrente*Math.cos((omega*i)+ Math.toRadians(anguloCorrente));
			valueTens = amplitudeTensao*Math.cos((omega*i)+ Math.toRadians(anguloTensao));
			value = Math.abs(valueTens)*Math.abs(valueCorr)*Math.sin(Math.toRadians(angulo));
			result.add(value);
		}
		return result;
	}
	
	public double getPotenciaAtiva (double amplitudeTensao, double anguloTensao , double amplitudeCorrente, double anguloCorrente) {
		double value = 0.0;
		anguloTensao = Math.toRadians(anguloTensao);
		anguloCorrente = Math.toRadians(anguloCorrente);
		value = amplitudeTensao*amplitudeCorrente*Math.cos(anguloTensao - anguloCorrente); 
		return value;
	}
	
	public double getPotenciaReativa(double amplitudeTensao, double anguloTensao , double amplitudeCorrente, double anguloCorrente) {
		double value = 0.0;
		anguloTensao = Math.toRadians(anguloTensao);
		anguloCorrente = Math.toRadians(anguloCorrente);
		value = amplitudeTensao*amplitudeCorrente*Math.sin(anguloTensao - anguloCorrente); 
		return value;
	}
	
	public double getFatorDePotencia(double anguloTensao, double anguloCorrente){
		double value = 0.0;
		anguloTensao = Math.toDegrees(anguloTensao);
		anguloCorrente = Math.toRadians(anguloCorrente);
		value = Math.cos(anguloTensao - anguloCorrente);
		return value;
	}
	
	public double getPotenciaAparente(double potenciaReativa, double potenciaAtiva) {
		double value = 0.0;
		value = Math.sqrt(Math.pow(potenciaAtiva, 2) + Math.pow(potenciaReativa, 2));
		return value;
	}
}
