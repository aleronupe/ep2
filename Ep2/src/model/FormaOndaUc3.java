package model;

import java.util.ArrayList;

public class FormaOndaUc3 extends FormaOndaUc2 {
	
	double amplitudeFundamental;
	double anguloFundamental;
	
	
	public ArrayList<Double> getFourrierSeries(double angFund, double ampFund, double angHar1, double ampHar1, int ordHar1, double angHar2, double ampHar2, int ordHar2, double angHar3, double ampHar3, int ordHar3, double angHar4, double ampHar4, int ordHar4 , double angHar5, double ampHar5, int ordHar5, double angHar6, double ampHar6, int ordHar6){
		double i = 0.0;
		double value = 0.0;
		ArrayList<Double> result = new ArrayList<Double>();
		for(i = 0.0; i < 100; i++) {
			value = 0.0;
			value = ampFund*Math.cos((omega*i)+ Math.toRadians(angFund));
			value += ampHar1*Math.cos((ordHar1*omega*i)+ Math.toRadians(angHar1));
			value += ampHar2*Math.cos((ordHar2*omega*i)+ Math.toRadians(angHar2));
			value += ampHar3*Math.cos((ordHar3*omega*i)+ Math.toRadians(angHar3));
			value += ampHar4*Math.cos((ordHar4*omega*i)+ Math.toRadians(angHar4));
			value += ampHar5*Math.cos((ordHar5*omega*i)+ Math.toRadians(angHar5));
			value += ampHar6*Math.cos((ordHar6*omega*i)+ Math.toRadians(angHar6));
			result.add(value);
		}
		return result;
	}
	
	
	public ArrayList<Double> getHarmonicoList (double amplitude, double angulo, int harmonico){
		double i = 0.0;
		double value = 0.0;
		ArrayList<Double> result = new ArrayList<Double>();
		for(i = 0.0; i < 100; i++) {
			value = amplitude*Math.cos((harmonico*omega*i)+ Math.toRadians(angulo));
			result.add(value);
		}
		
		return result;
	}

}
