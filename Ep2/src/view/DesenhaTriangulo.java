package view;

import java.awt.BasicStroke;
import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.RenderingHints;
import java.awt.Stroke;

import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class DesenhaTriangulo extends JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	//private int width = 500;
    //private int heigth = 300;
    private int padding = 30;
    private int labelPadding = 25;
    private Color lineColor = new Color(44, 102, 230, 180);
    private Color pointColor = new Color(100, 100, 100, 180);
    private Color gridColor = new Color(200, 200, 200, 200);
    private static final Stroke GRAPH_STROKE = new BasicStroke(2f);

    private List<Double> scores;
    int potReativa;
    int potAtiva;

    public DesenhaTriangulo (int potAtiva, int potReativa) {
        this.potAtiva = (potAtiva*155)/22000;
        this.potReativa = (potReativa*155)/22000;
        
       
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // draw white background
        g2.setColor(Color.WHITE);
        g2.fillRect(padding + labelPadding, padding, getWidth() - (2 * padding) - labelPadding, getHeight() - 2 * padding - labelPadding);
        g2.setColor(Color.BLACK);

        // create x and y axes
        
        g2.drawLine( (getWidth() + 35 - labelPadding)/2, padding, (getWidth() + 35 - labelPadding)/2 , getHeight() - 30 - labelPadding);
        g2.drawLine(padding + labelPadding,  (getHeight() + 4 - labelPadding)/2, getWidth() - 5 - labelPadding, (getHeight() + 4 - labelPadding)/2);

        g2.setStroke(new BasicStroke(3));
        //desenha potAtiva
        g2.setColor(Color.RED);
        g2.drawLine( (getWidth() + 35 - labelPadding)/2,  (getHeight() + 4 - labelPadding)/2, ((getWidth() + 35 - labelPadding)/2) + potAtiva ,  (getHeight() + 4 - labelPadding)/2);
        
        //desenha potReativa
        g2.setColor(Color.GREEN);
        g2.drawLine( ((getWidth() + 35 - labelPadding)/2) + potAtiva,  (getHeight() + 4 - labelPadding)/2, (getWidth() + 35 - labelPadding)/2 + potAtiva,  ((getHeight() + 4 - labelPadding)/2) - potReativa);
        
        //desenha Aparente
        g2.setColor(Color.BLUE);
        g2.drawLine( (getWidth() + 35 - labelPadding)/2 ,  (getHeight() + 4 - labelPadding)/2, ((getWidth() + 35 - labelPadding)/2) + potAtiva ,  ((getHeight() + 4 - labelPadding)/2) - potReativa);
        

    }


    public void setPoints(int potAtiva, int potReativa) {    	
    	 this.potAtiva = (potAtiva*128)/22000;
         this.potReativa = (potReativa*128)/22000;
         if(this.potAtiva < 13 && this.potAtiva > 0) {
        	 this.potAtiva = this.potAtiva+13;
         }
         else if(this.potAtiva > -13 && this.potAtiva < 0) {
        	 this.potAtiva = this.potAtiva-13;
         }
         if(this.potReativa < 13 && this.potReativa > 0) {
        	 this.potReativa = this.potReativa+22;
         }
         else if(this.potReativa > -13 && this.potReativa < 0) {
        	 this.potReativa = this.potReativa-22;
         }
        //JOptionPane.showMessageDialog(null, this.potReativa);
        invalidate();
        this.repaint();
    }

    public List<Double> getScores() {
        return scores;
    }
    
}

