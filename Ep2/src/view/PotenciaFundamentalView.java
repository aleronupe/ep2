package view;


import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.IniciaGraficosCont;
import controller.PotenciaFundamentalCont;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.Color;
import java.awt.Font;

public class PotenciaFundamentalView {
	
	private JFrame frameUc2;
	private JPanel ondaTensao;
	private JPanel ondaCorrente;
	private JPanel ondaPotencia;
	private JPanel triangulo;
	private JLabel corrente;
	private JLabel tensao;
	private JLabel labelOndaTensao;
	private JLabel labelOndaCorrente;
	private JLabel labelOndaPotencia;
	private JLabel labelTriangulo;
	private JFormattedTextField ampTens;
	private JLabel labelAmpTens;
	private JFormattedTextField angTens;
	private JLabel labelAngTens;
	private JFormattedTextField ampCorr;
	private JLabel labelAmpCorr;
	private JFormattedTextField angCorr;
	private JLabel labelAngCorr;
	private JButton okTensao;
	private JButton okCorrente;
	private JButton voltar;
	public JLabel labelPotenciaAtiva;
	public JLabel labelPotenciaReativa;
	public JLabel labelPotenciaAparente;
	public JLabel labelFatorDePotencia;
	private JLabel valorPotenciaAtiva;
	private JLabel valorPotenciaReativa;
	private JLabel valorPotenciaAparente;
	private JLabel fatorDePotencia;
	public PotenciaFundamentalCont potFundCont;
	public DesenhaGrafico grafOndaTensao;
	public DesenhaGrafico grafOndaCorrente;
	public DesenhaGrafico grafPotenciaInstantanea;
	public DesenhaTriangulo grafTriangulo;
	public IniciaGraficosCont inicializador;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JLabel lblReativa;
	private JLabel lblReativa_1;
	private JLabel lblAparente;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void mostraPotFund() {
		
		frameUc2 = new JFrame("Fluxo de Potência Fundamental");
		frameUc2.setSize(900, 1200);
		frameUc2.setVisible(true);
		frameUc2.getContentPane().setLayout(null);
		frameUc2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		ondaTensao = new JPanel();
		ondaTensao.setBackground(new Color(238, 238, 238));
		ondaTensao.setLocation(12, 0);
		ondaTensao.setSize(876, 194);
		ampTens = new JFormattedTextField("0");
		ampTens.setLocation(30, 88);
		ampTens.setSize(48, 25);
		angTens = new JFormattedTextField("0");
		angTens.setLocation(115, 88);
		angTens.setSize(48, 25);
		okTensao = new JButton("Ok");
		okTensao.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
		okTensao.setLocation(73, 125);
		okTensao.setSize(48, 25);
		
		ondaCorrente = new JPanel();
		ondaCorrente.setLocation(12, 219);
		ondaCorrente.setSize(876, 194);

		
		ondaPotencia = new JPanel();
		ondaPotencia.setLocation(12, 414);
		ondaPotencia.setSize(438, 290);
		labelOndaPotencia = new JLabel("Forma da Onda de Potência");
		labelOndaPotencia.setBounds(128, 38, 201, 30);
		
		labelPotenciaAtiva = new JLabel("Potencia Ativa");
		labelPotenciaAtiva.setBounds(12, 96, 201, 30);
		labelPotenciaReativa = new JLabel("Potencia Reativa");
		labelPotenciaReativa.setBounds(12, 54, 201, 30);
		labelPotenciaAparente = new JLabel("Potencia Aparente");
		labelPotenciaAparente.setBounds(12, 138, 201, 30);
		labelFatorDePotencia = new JLabel("Fator de Potencia");
		labelFatorDePotencia.setBounds(12, 12, 201, 30);
		
		
		valorPotenciaAtiva = new JLabel("0");
		valorPotenciaAtiva.setBounds(166, 96, 201, 30);
		valorPotenciaReativa = new JLabel("0");
		valorPotenciaReativa.setBounds(166, 54, 201, 30);
		valorPotenciaAparente = new JLabel("0");
		valorPotenciaAparente.setBounds(166, 138, 201, 30);
		fatorDePotencia = new JLabel("0");
		fatorDePotencia.setBackground(Color.WHITE);
		fatorDePotencia.setBounds(166, 12, 201, 30);
		
		triangulo = new JPanel();
		triangulo.setLocation(456, 414);
		triangulo.setSize(432, 290);
		
		inicializador = new IniciaGraficosCont();
		
		grafOndaCorrente = new DesenhaGrafico(inicializador.criaList());
		grafOndaCorrente.setBounds(408, 12, 456, 193);
		grafOndaTensao = new DesenhaGrafico(inicializador.criaList());
		grafOndaTensao.setBounds(408, 12, 456, 193);
		grafPotenciaInstantanea = new DesenhaGrafico(inicializador.criaList());
		grafPotenciaInstantanea.setLocation(12, 58);
		grafPotenciaInstantanea.setSize(414, 220);
		grafTriangulo = new DesenhaTriangulo(0, 0);
		grafTriangulo.setLocation(12, 12);
		grafTriangulo.setSize(326, 292);
		potFundCont = new PotenciaFundamentalCont();
		okTensao.setActionCommand("Tensao");
		okTensao.addActionListener(potFundCont);
		potFundCont.CalculosGerais(grafPotenciaInstantanea, grafTriangulo, valorPotenciaAtiva, valorPotenciaReativa, valorPotenciaAparente, fatorDePotencia);
		potFundCont.CalculosTensao(okTensao, angTens, ampTens, grafOndaTensao);

		
		labelOndaTensao = new JLabel("Forma da Onda de Tensao");
		grafOndaTensao.add(labelOndaTensao);
	

	
		
		frameUc2.getContentPane().add(ondaTensao);
		ondaTensao.add(okTensao);
		ondaTensao.add(ampTens);
		ondaTensao.add(angTens);
		ondaTensao.add(grafOndaTensao);
		ondaTensao.setLayout(null);
		corrente = new JLabel("Corrente");
		corrente.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 13));
		corrente.setBounds(273, 28, 74, 25);
		ondaTensao.add(corrente);
		tensao = new JLabel("Tensao");
		tensao.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 13));
		tensao.setBounds (68, 28, 74, 25);
		ondaTensao.add(tensao);
		labelAmpTens = new JLabel("Amplitude");
		labelAmpTens.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 12));
		labelAmpTens.setBounds(12, 61, 74, 25);
		ondaTensao.add(labelAmpTens);
		labelAngTens = new JLabel("Ângulo");
		labelAngTens.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 12));
		labelAngTens.setBounds(119, 61, 65, 25);
		ondaTensao.add(labelAngTens);
		okCorrente = new JButton("Ok");
		okCorrente.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
		okCorrente.setBounds(273, 125, 48, 25);
		ondaTensao.add(okCorrente);
		okCorrente.setActionCommand("Corrente");
		okCorrente.addActionListener(potFundCont);
		
		voltar = new JButton("Voltar");
		voltar.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
		voltar.setBounds(350, 250, 76, 25);
		triangulo.add(voltar);
		voltar.setActionCommand("Voltar");
		voltar.addActionListener(potFundCont);
		
		ampCorr = new JFormattedTextField("0");
		ampCorr.setBounds(230, 88, 48, 25);
		ondaTensao.add(ampCorr);
		angCorr = new JFormattedTextField("0");
		angCorr.setBounds(315, 88, 48, 25);
		ondaTensao.add(angCorr);
		potFundCont.CalculosCorrente(okCorrente, angCorr, ampCorr, grafOndaCorrente);
		labelAmpCorr = new JLabel("Amplitude");
		labelAmpCorr.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 12));
		labelAmpCorr.setBounds(219, 56, 77, 34);
		ondaTensao.add(labelAmpCorr);
		labelAngCorr = new JLabel("Ângulo");
		labelAngCorr.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 12));
		labelAngCorr.setBounds(321, 56, 60, 34);
		ondaTensao.add(labelAngCorr);
		
		frameUc2.getContentPane().add(ondaCorrente);
		ondaCorrente.setLayout(null);
		ondaCorrente.add(labelFatorDePotencia);
		ondaCorrente.add(labelPotenciaReativa);
		ondaCorrente.add(labelPotenciaAtiva);
		ondaCorrente.add(labelPotenciaAparente);
		ondaCorrente.add(fatorDePotencia);
		ondaCorrente.add(valorPotenciaReativa);
		ondaCorrente.add(valorPotenciaAtiva);
		ondaCorrente.add(valorPotenciaAparente);

		frameUc2.getContentPane().add(ondaPotencia);
		ondaPotencia.setLayout(null);
		ondaPotencia.add(labelOndaPotencia);
		ondaPotencia.add(grafPotenciaInstantanea);
		
		frameUc2.getContentPane().add(triangulo);
		triangulo.setLayout(null);
		triangulo.add(grafTriangulo);
		grafTriangulo.setLayout(null);
		labelTriangulo = new JLabel("Triângulo de Potências");
		labelTriangulo.setBounds(95, 5, 163, 15);
		grafTriangulo.add(labelTriangulo);
		
		JLabel lblNewLabel_1 = new JLabel("Potencia Reativa (VAR)");
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 9));
		lblNewLabel_1.setBounds(172, 32, 115, 15);
		grafTriangulo.add(lblNewLabel_1);
		
		lblNewLabel = new JLabel("Potencia Ativa(W)");
		lblNewLabel.setBounds(54, 141, 125, 15);
		grafTriangulo.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 9));
		
		lblNewLabel_2 = new JLabel("Potencia");
		lblNewLabel_2.setForeground(Color.RED);
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD, 9));
		lblNewLabel_2.setBounds(350, 37, 70, 15);
		triangulo.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Potencia");
		lblNewLabel_3.setForeground(Color.GREEN);
		lblNewLabel_3.setFont(new Font("Dialog", Font.BOLD, 9));
		lblNewLabel_3.setBounds(350, 99, 70, 15);
		triangulo.add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Potencia");
		lblNewLabel_4.setForeground(Color.BLUE);
		lblNewLabel_4.setFont(new Font("Dialog", Font.BOLD, 9));
		lblNewLabel_4.setBounds(350, 160, 70, 15);
		triangulo.add(lblNewLabel_4);
		
		lblReativa = new JLabel("Ativa");
		lblReativa.setBounds(350, 50, 70, 15);
		triangulo.add(lblReativa);
		lblReativa.setForeground(Color.RED);
		lblReativa.setFont(new Font("Dialog", Font.BOLD, 9));
		
		lblReativa_1 = new JLabel("Reativa");
		lblReativa_1.setForeground(Color.GREEN);
		lblReativa_1.setFont(new Font("Dialog", Font.BOLD, 9));
		lblReativa_1.setBounds(350, 115, 70, 15);
		triangulo.add(lblReativa_1);
		
		lblAparente = new JLabel("Aparente");
		lblAparente.setForeground(Color.BLUE);
		lblAparente.setFont(new Font("Dialog", Font.BOLD, 9));
		lblAparente.setBounds(350, 177, 70, 15);
		triangulo.add(lblAparente);
		
		label = new JLabel("___________");
		label.setForeground(Color.RED);
		label.setFont(new Font("Lato Black", Font.BOLD | Font.ITALIC, 10));
		label.setBounds(350, 64, 70, 15);
		triangulo.add(label);
		
		label_1 = new JLabel("___________");
		label_1.setForeground(Color.GREEN);
		label_1.setFont(new Font("Lato Black", Font.BOLD | Font.ITALIC, 10));
		label_1.setBounds(350, 130, 70, 15);
		triangulo.add(label_1);
		
		label_2 = new JLabel("___________");
		label_2.setForeground(Color.BLUE);
		label_2.setFont(new Font("Lato Black", Font.BOLD | Font.ITALIC, 10));
		label_2.setBounds(356, 195, 70, 15);
		triangulo.add(label_2);
		ondaCorrente.add(grafOndaCorrente);
		labelOndaCorrente = new JLabel("Forma da Onda de Corrente");
		grafOndaCorrente.add(labelOndaCorrente);
		
		potFundCont.PassaFrame(frameUc2);
		
		frameUc2.setVisible(true);
	}
}
