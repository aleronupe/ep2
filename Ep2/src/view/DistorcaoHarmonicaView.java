package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import controller.DistorcaoHarmonicaCont;
import controller.IniciaGraficosCont;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class DistorcaoHarmonicaView {

	private JFrame frameUc3;
	private JPanel seletores;
	private JPanel fourrier;
	private JPanel adicionaGraficos;
	private JScrollPane scrollGraficos;
	private JLabel labelAnguloFundamental;
	private JLabel labelAmplitudeFundamental;
	private JLabel labelParImparHarmonicos;
	private JLabel labelnumeroHarmonicos;
	private JLabel labelSerieDeFourrier;
	private JLabel serieDeFourrier;
	private JLabel labelOrdHar1;
	private JLabel labelOrdHar2;
	private JLabel labelOrdHar3;
	private JLabel labelOrdHar4;
	private JLabel labelOrdHar5;
	private JLabel labelOrdHar6;
	private JLabel labelAngHar1;
	private JLabel labelAngHar2;
	private JLabel labelAngHar3;
	private JLabel labelAngHar4;
	private JLabel labelAngHar5;
	private JLabel labelAngHar6;
	private JLabel labelAmpHar1;
	private JLabel labelAmpHar2;
	private JLabel labelAmpHar3;
	private JLabel labelAmpHar4;
	private JLabel labelAmpHar5;
	private JLabel labelAmpHar6;
	private JButton okSeletores;
	private JRadioButton imparHarmonico;
	private JRadioButton parHarmonico;
	private JFormattedTextField anguloFundamental;
	private JSpinner numeroHarmonicos;
	private JSpinner ordemHarmonicos1;
	private JSpinner ordemHarmonicos2;
	private JSpinner ordemHarmonicos3;
	private JSpinner ordemHarmonicos4;
	private JSpinner ordemHarmonicos5;
	private JSpinner ordemHarmonicos6;
	private JFormattedTextField anguloHarmonicos1;
	private JFormattedTextField anguloHarmonicos2;
	private JFormattedTextField anguloHarmonicos3;
	private JFormattedTextField anguloHarmonicos4;
	private JFormattedTextField anguloHarmonicos5;
	private JFormattedTextField anguloHarmonicos6;
	private JFormattedTextField amplitudeFundamental;
	private JFormattedTextField amplitudeHarmonicos1;
	private JFormattedTextField amplitudeHarmonicos2;
	private JFormattedTextField amplitudeHarmonicos3;
	private JFormattedTextField amplitudeHarmonicos4;
	private JFormattedTextField amplitudeHarmonicos5;
	private JFormattedTextField amplitudeHarmonicos6;
	private JButton okButtonGraf1;
	private JButton okButtonGraf2;
	private JButton okButtonGraf3;
	private JButton okButtonGraf4;
	private JButton okButtonGraf5;
	private JButton okButtonGraf6;
	public DesenhaGrafico grafOndaFundamental;
	public DesenhaGrafico grafOndaResultante;
	public DesenhaGrafico grafOrdemHarmonica1;
	public DesenhaGrafico grafOrdemHarmonica2;
	public DesenhaGrafico grafOrdemHarmonica3;
	public DesenhaGrafico grafOrdemHarmonica4;
	public DesenhaGrafico grafOrdemHarmonica5;
	public DesenhaGrafico grafOrdemHarmonica6;
	public IniciaGraficosCont inicializador;
	public DistorcaoHarmonicaCont distHarCont;
	private JButton voltarButton;
	private JLabel lblNewLabel;
	private JLabel lblFormaDaOnda;
	private JLabel lblOrdemHarmonica;
	private JLabel lblOrdemHarmonica2;
	private JLabel lblOrdemHarmonica3;
	private JLabel lblOrdemHarmonica4;
	private JLabel lblOrdemHarmonica5;
	private JLabel lblOrdemHarmonica6;
		/**
		 * @wbp.parser.entryPoint
		 */
		public void mostraDistHar() {
			
			frameUc3 = new JFrame("Distorção Harmônica");
			frameUc3.setSize(1050, 900);
			frameUc3.setVisible(true);
			frameUc3.getContentPane().setLayout(null);
			frameUc3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
			adicionaGraficos = new JPanel();
			adicionaGraficos.setLayout(null);
			adicionaGraficos.setPreferredSize(new Dimension (570, 1600));
			
			scrollGraficos = new JScrollPane();
			scrollGraficos.setLocation(354, 12);
			scrollGraficos.setSize(660, 333);
			scrollGraficos.setPreferredSize(new Dimension(592, 400));
			scrollGraficos.setViewportView(adicionaGraficos);
			frameUc3.getContentPane().add(scrollGraficos);
			

			fourrier = new JPanel();
			fourrier.setLocation(12, 376);
			fourrier.setSize(1026, 340);
			
			seletores = new JPanel();
			seletores.setBackground(new Color(238, 238, 238));
			seletores.setLocation(12, 24);
			seletores.setSize(338, 340);
			
			parHarmonico = new JRadioButton("Par", false);
			parHarmonico.setFont(new Font("Dialog", Font.BOLD, 11));
			parHarmonico.setBounds(101, 230, 49, 23);
			
			imparHarmonico = new JRadioButton("Impar", false);
			imparHarmonico.setFont(new Font("Dialog", Font.BOLD, 11));
			imparHarmonico.setBounds(180, 230, 65, 23);
			
			ButtonGroup grupo1 = new ButtonGroup();
			
			grupo1.add(parHarmonico);
			grupo1.add(imparHarmonico);
			
			
			inicializador = new IniciaGraficosCont();
			
			numeroHarmonicos = new JSpinner();
			numeroHarmonicos.setFont(new Font("Dialog", Font.BOLD, 11));
			numeroHarmonicos.setBounds(143, 165, 41, 20);
			
			anguloFundamental = new JFormattedTextField("0");
			anguloFundamental.setFont(new Font("Dialog", Font.BOLD, 11));
			anguloFundamental.setBounds(143, 40, 41, 20);
			
			amplitudeFundamental = new JFormattedTextField("0");
			amplitudeFundamental.setBounds(143, 110, 41, 20);
			amplitudeFundamental.setFont(new Font("Dialog", Font.BOLD, 11));
			
			
			labelParImparHarmonicos = new JLabel("Harmônicos");
			labelParImparHarmonicos.setFont(new Font("Dialog", Font.BOLD, 11));
			labelParImparHarmonicos.setBounds(126, 200, 104, 15);
			
			labelAnguloFundamental = new JLabel("Angulo da Onda Fundamental");
			labelAnguloFundamental.setFont(new Font("Dialog", Font.BOLD, 11));
			labelAnguloFundamental.setBounds(66, 12, 223, 15);
			
			labelAmplitudeFundamental = new JLabel("Amplitude da Onda Fundamental");
			labelAmplitudeFundamental.setFont(new Font("Dialog", Font.BOLD, 11));
			labelAmplitudeFundamental.setBounds(66, 80, 223, 15);
			
			labelnumeroHarmonicos = new JLabel("Número de Harmônicos");
			labelnumeroHarmonicos.setFont(new Font("Dialog", Font.BOLD, 11));
			labelnumeroHarmonicos.setBounds(85, 140, 223, 15);
			
			
			labelSerieDeFourrier = new JLabel("Série de Fourrier Amplitude-Fase");
			labelSerieDeFourrier.setFont(new Font("Dialog", Font.BOLD, 12));
			labelSerieDeFourrier.setBounds(400, 5, 234, 15);
			
			serieDeFourrier = new JLabel("f(t) =");
			serieDeFourrier.setFont(new Font("Dialog", Font.ITALIC, 12));
			serieDeFourrier.setBounds(59, 37, 874, 15);
			
			
			grafOndaFundamental = new DesenhaGrafico(inicializador.criaList());
			grafOndaFundamental.setBounds(12, 80, 436, 203);
			
			grafOndaResultante = new DesenhaGrafico(inicializador.criaList());
			grafOndaResultante.setBounds(506, 80, 433, 203);
			seletores.setLayout(null);
			seletores.add(numeroHarmonicos);
			seletores.add(labelParImparHarmonicos);
			seletores.add(labelnumeroHarmonicos);
			seletores.add(labelAnguloFundamental);
			seletores.add(labelAmplitudeFundamental);
			seletores.add(imparHarmonico);
			seletores.add(anguloFundamental);
			seletores.add(amplitudeFundamental);
			
			okSeletores = new JButton("Ok");
			okSeletores.setFont(new Font("Dialog", Font.BOLD, 11));
			okSeletores.setBounds(136, 280, 56, 24);
			okSeletores.setActionCommand("Seletor");
			
			seletores.add(okSeletores);
			seletores.add(parHarmonico);
			fourrier.setLayout(null);
			fourrier.add(labelSerieDeFourrier);
			fourrier.add(serieDeFourrier);
			fourrier.add(grafOndaFundamental);
			fourrier.add(grafOndaResultante);;
			frameUc3.getContentPane().add(seletores);
			frameUc3.getContentPane().add(fourrier);
			
			grafOrdemHarmonica1 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica1.setBounds(185, 80, 437, 227);
			
			grafOrdemHarmonica2 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica2.setBounds(185, 330, 437, 227);
			
			grafOrdemHarmonica3 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica3.setBounds(185, 580, 437, 227);
	
			grafOrdemHarmonica4 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica4.setBounds(185, 830, 437, 227);
			
			grafOrdemHarmonica5 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica5.setBounds(185, 1080, 437, 227);
			
			grafOrdemHarmonica6 = new DesenhaGrafico(inicializador.criaListHar());
			grafOrdemHarmonica6.setBounds(185, 1330, 437, 227);
			
			ordemHarmonicos1 = new JSpinner();
			ordemHarmonicos1.setBounds(40, 217, 41, 20);
			ordemHarmonicos1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			ordemHarmonicos2 = new JSpinner();
			ordemHarmonicos2.setBounds(40, 444, 41, 20);
			ordemHarmonicos2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			adicionaGraficos.setLayout(null);
			adicionaGraficos.add(ordemHarmonicos2);
			
			ordemHarmonicos3 = new JSpinner();
			ordemHarmonicos3.setBounds(40, 694, 41, 20);
			adicionaGraficos.add(ordemHarmonicos3);
			ordemHarmonicos3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			ordemHarmonicos4 = new JSpinner();
			ordemHarmonicos4.setBounds(40, 944, 41, 20);
			adicionaGraficos.add(ordemHarmonicos4);
			ordemHarmonicos4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			ordemHarmonicos5 = new JSpinner();
			ordemHarmonicos5.setBounds(40, 1194, 41, 20);
			adicionaGraficos.add(ordemHarmonicos5);
			ordemHarmonicos5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			ordemHarmonicos6 = new JSpinner();
			ordemHarmonicos6.setBounds(40, 1444, 41, 20);
			adicionaGraficos.add(ordemHarmonicos6);
			ordemHarmonicos6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			adicionaGraficos.add(grafOrdemHarmonica1);
			adicionaGraficos.add(grafOrdemHarmonica2);
			adicionaGraficos.add(grafOrdemHarmonica3);
			adicionaGraficos.add(grafOrdemHarmonica4);
			adicionaGraficos.add(grafOrdemHarmonica5);
			adicionaGraficos.add(ordemHarmonicos1);
			adicionaGraficos.add(grafOrdemHarmonica6); 
			
			anguloHarmonicos1 = new JFormattedTextField("0");
			anguloHarmonicos1.setBounds(130, 217, 41, 20);
			anguloHarmonicos1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			adicionaGraficos.add(anguloHarmonicos1);
			
			anguloHarmonicos2 = new JFormattedTextField("0");
			anguloHarmonicos2.setBounds(130, 444, 41, 20);
			anguloHarmonicos2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			adicionaGraficos.add(anguloHarmonicos2);
			
			
			anguloHarmonicos3 = new JFormattedTextField("0");
			anguloHarmonicos3.setBounds(130, 694, 41, 20);
			adicionaGraficos.add(anguloHarmonicos3);
			anguloHarmonicos3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			anguloHarmonicos4 = new JFormattedTextField("0");
			anguloHarmonicos4.setBounds(130, 944, 41, 20);
			adicionaGraficos.add(anguloHarmonicos4);
			anguloHarmonicos4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			anguloHarmonicos5 = new JFormattedTextField("0");
			anguloHarmonicos5.setBounds(130, 1194, 41, 20);
			adicionaGraficos.add(anguloHarmonicos5);
			anguloHarmonicos5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			anguloHarmonicos6 = new JFormattedTextField("0");
			anguloHarmonicos6.setBounds(130, 1444, 41, 20);
			adicionaGraficos.add(anguloHarmonicos6);
			anguloHarmonicos6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			okButtonGraf1 = new JButton("Simular");
			okButtonGraf1.setBounds(65, 269, 90, 24);
			okButtonGraf1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			okButtonGraf1.setVisible(false);
			okButtonGraf1.setActionCommand("Sim1");
			adicionaGraficos.add(okButtonGraf1);
			
			
			okButtonGraf2 = new JButton("Simular");
			okButtonGraf2.setBounds(65, 494, 90, 24);
			okButtonGraf2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			okButtonGraf2.setVisible(false);
			okButtonGraf2.setActionCommand("Sim2");
			adicionaGraficos.add(okButtonGraf2);
			
			
			okButtonGraf3 = new JButton("Simular");
			okButtonGraf3.setBounds(65, 754, 90, 24);
			okButtonGraf3.setActionCommand("Sim3");
			okButtonGraf3.setVisible(false);
			adicionaGraficos.add(okButtonGraf3);
			okButtonGraf3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			okButtonGraf4 = new JButton("Simular");
			okButtonGraf4.setBounds(65, 994, 90, 24);
			adicionaGraficos.add(okButtonGraf4);
			okButtonGraf4.setActionCommand("Sim4");
			okButtonGraf4.setVisible(false);
			okButtonGraf4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			okButtonGraf5 = new JButton("Simular");
			okButtonGraf5.setBounds(65, 1244, 90, 24);
			adicionaGraficos.add(okButtonGraf5);
			okButtonGraf5.setActionCommand("Sim5");
			okButtonGraf5.setVisible(false);
			okButtonGraf5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			okButtonGraf6 = new JButton("Simular");
			okButtonGraf6.setBounds(65, 1494, 90, 24);
			adicionaGraficos.add(okButtonGraf6);
			okButtonGraf6.setActionCommand("Sim6");
			okButtonGraf6.setVisible(false);
			okButtonGraf6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			labelAngHar1 = new JLabel("Ângulo");
			labelAngHar1.setBounds(127, 192, 72, 20);
			labelAngHar1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			adicionaGraficos.add(labelAngHar1);
			
			labelAngHar2 = new JLabel("Ângulo");
			labelAngHar2.setBounds(127, 419, 72, 20);
			labelAngHar2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			adicionaGraficos.add(labelAngHar2);
			
			
			labelAngHar3 = new JLabel("Ângulo");
			labelAngHar3.setBounds(127, 669, 72, 20);
			adicionaGraficos.add(labelAngHar3);
			labelAngHar3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelAngHar4 = new JLabel("Ângulo");
			labelAngHar4.setBounds(127, 919, 72, 20);
			adicionaGraficos.add(labelAngHar4);
			labelAngHar4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			labelAngHar5 = new JLabel("Ângulo");
			labelAngHar5.setBounds(127, 1169, 72, 20);
			adicionaGraficos.add(labelAngHar5);
			labelAngHar5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelAngHar6 = new JLabel("Ângulo");
			labelAngHar6.setBounds(127, 1419, 72, 20);
			adicionaGraficos.add(labelAngHar6);
			labelAngHar6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelOrdHar1 = new JLabel("Ordem");
			labelOrdHar1.setBounds(40, 192, 41, 20);
			adicionaGraficos.add(labelOrdHar1);
			labelOrdHar1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelOrdHar2 = new JLabel("Ordem");
			labelOrdHar2.setBounds(40, 419, 41, 20);
			labelOrdHar2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			adicionaGraficos.setLayout(null);
			adicionaGraficos.add(labelOrdHar2);
			
			labelOrdHar3 = new JLabel("Ordem");
			labelOrdHar3.setBounds(40, 669, 41, 20);
			adicionaGraficos.add(labelOrdHar3);
			labelOrdHar3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelOrdHar4 = new JLabel("Ordem");
			labelOrdHar4.setBounds(40, 919, 41, 20);
			adicionaGraficos.add(labelOrdHar4);
			labelOrdHar4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			labelOrdHar5 = new JLabel("Ordem");
			labelOrdHar5.setBounds(40, 1169, 41, 20);
			adicionaGraficos.add(labelOrdHar5);
			labelOrdHar5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelOrdHar6 = new JLabel("Ordem");
			labelOrdHar6.setBounds(40, 1419, 41, 20);
			adicionaGraficos.add(labelOrdHar6);
			labelOrdHar6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			labelAmpHar1 = new JLabel("Amplitude");
			labelAmpHar1.setBounds(70, 129, 76, 20);
			adicionaGraficos.add(labelAmpHar1);
			labelAmpHar1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelAmpHar2 = new JLabel("Amplitude");
			labelAmpHar2.setBounds(70, 364, 76, 20);
			adicionaGraficos.add(labelAmpHar2);
			labelAmpHar2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			

			labelAmpHar3 = new JLabel("Amplitude");
			labelAmpHar3.setBounds(70, 619, 76, 20);
			adicionaGraficos.add(labelAmpHar3);
			labelAmpHar3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelAmpHar4 = new JLabel("Amplitude");
			labelAmpHar4.setBounds(70, 869, 76, 20);
			adicionaGraficos.add(labelAmpHar4);
			labelAmpHar4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			labelAmpHar5 = new JLabel("Amplitude");
			labelAmpHar5.setBounds(70, 1119, 76, 20);
			adicionaGraficos.add(labelAmpHar5);
			labelAmpHar5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			labelAmpHar6 = new JLabel("Amplitude");
			labelAmpHar6.setBounds(70, 1369, 76, 20);
			adicionaGraficos.add(labelAmpHar6);
			labelAmpHar6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			
			amplitudeHarmonicos1 = new JFormattedTextField("0");
			amplitudeHarmonicos1.setBounds(85, 154, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos1);
			amplitudeHarmonicos1.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			amplitudeHarmonicos2 = new JFormattedTextField("0");
			amplitudeHarmonicos2.setBounds(85, 389, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos2);
			amplitudeHarmonicos2.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			

			amplitudeHarmonicos3 = new JFormattedTextField("0");
			amplitudeHarmonicos3.setBounds(85, 644, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos3);
			amplitudeHarmonicos3.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			amplitudeHarmonicos4 = new JFormattedTextField("0");
			amplitudeHarmonicos4.setBounds(85, 894, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos4);
			amplitudeHarmonicos4.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			amplitudeHarmonicos5 = new JFormattedTextField("0");
			amplitudeHarmonicos5.setBounds(85, 1144, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos5);
			amplitudeHarmonicos5.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			
			amplitudeHarmonicos6 = new JFormattedTextField("0");
			amplitudeHarmonicos6.setBounds(85, 1394, 41, 20);
			adicionaGraficos.add(amplitudeHarmonicos6);
			amplitudeHarmonicos6.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
			

			distHarCont = new DistorcaoHarmonicaCont();
			distHarCont.passaButtons(okButtonGraf1, okButtonGraf2, okButtonGraf3, okButtonGraf4, okButtonGraf5, okButtonGraf6);
			distHarCont.passaGraficosHar(grafOrdemHarmonica1, grafOrdemHarmonica2, grafOrdemHarmonica3, grafOrdemHarmonica4, grafOrdemHarmonica5, grafOrdemHarmonica6);
			
			lblOrdemHarmonica = new JLabel("1º Harmonico");
			grafOrdemHarmonica1.add(lblOrdemHarmonica);
			lblOrdemHarmonica2 = new JLabel("2º Harmonico");
			grafOrdemHarmonica2.add(lblOrdemHarmonica2);
			lblOrdemHarmonica3 = new JLabel("3º Harmonico");
			grafOrdemHarmonica3.add(lblOrdemHarmonica3);
			lblOrdemHarmonica4 = new JLabel("4º Harmonico");
			grafOrdemHarmonica4.add(lblOrdemHarmonica4);
			lblOrdemHarmonica5 = new JLabel("5º Harmonico");
			grafOrdemHarmonica5.add(lblOrdemHarmonica5);
			lblOrdemHarmonica6 = new JLabel("6º Harmonico");
			grafOrdemHarmonica6.add(lblOrdemHarmonica6);
			distHarCont.passaLabelFour(grafOndaFundamental, grafOndaResultante, serieDeFourrier, okSeletores, numeroHarmonicos, anguloFundamental, amplitudeFundamental);
			
						voltarButton = new JButton("Voltar");
						voltarButton.setBounds(924, 294, 90, 24);
						fourrier.add(voltarButton);
						voltarButton.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 11));
						voltarButton.setActionCommand("Voltar");
						
						lblNewLabel = new JLabel("Forma da Onda Fundamental");
						lblNewLabel.setBounds(132, 280, 225, 15);
						fourrier.add(lblNewLabel);
						
						lblFormaDaOnda = new JLabel("Forma da Onda Distorcida Resultante");
						lblFormaDaOnda.setBounds(591, 280, 317, 15);
						fourrier.add(lblFormaDaOnda);
						voltarButton.addActionListener(distHarCont);
			distHarCont.passaTextField(amplitudeHarmonicos1, amplitudeHarmonicos2, amplitudeHarmonicos3, amplitudeHarmonicos4, amplitudeHarmonicos5, amplitudeHarmonicos6);
			distHarCont.passaAngulosHar(anguloHarmonicos1, anguloHarmonicos2, anguloHarmonicos3, anguloHarmonicos4, anguloHarmonicos5, anguloHarmonicos6);
			distHarCont.passaSpinnerOrd(ordemHarmonicos1, ordemHarmonicos2, ordemHarmonicos3, ordemHarmonicos4, ordemHarmonicos5, ordemHarmonicos6);
			distHarCont.passaFrame(frameUc3);
			
			okSeletores.addActionListener(distHarCont);
			okButtonGraf1.addActionListener(distHarCont);
			okButtonGraf2.addActionListener(distHarCont);
			okButtonGraf3.addActionListener(distHarCont);
			okButtonGraf4.addActionListener(distHarCont);
			okButtonGraf5.addActionListener(distHarCont);
			okButtonGraf6.addActionListener(distHarCont);
			
			frameUc3.setVisible(true);
		}
}
