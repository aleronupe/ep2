package view;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import controller.JanelaPrimariaCont;
import java.awt.Font;

public class JanelaPrimariaView {
	
	private JFrame framePrincipal;
	private JLabel labelPrincipal;
	private JPanel painelPrincipal;
	public JanelaPrimariaCont jancont;
	
	public void mostraInitPan() {
		framePrincipal = new JFrame("Aprenda QEE");
		framePrincipal.setSize(399, 483);
		framePrincipal.setVisible(true);
		framePrincipal.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		painelPrincipal = new JPanel();
		painelPrincipal.setLocation(12, 22);
		painelPrincipal.setSize(376, 449);
		
		labelPrincipal = new JLabel("Selecione a simulação desejada:");
		labelPrincipal.setFont(new Font("Dialog", Font.BOLD, 14));
		labelPrincipal.setBounds(46, 49, 318, 15);
		
		JRadioButton PotFund = new JRadioButton("Fluxo de Potência Fundamental", false);
		PotFund.setFont(new Font("Dialog", Font.BOLD, 12));
		PotFund.setBounds(69, 138, 325, 23);
		
		JRadioButton DistHar = new JRadioButton("Distorção Harmônica", false);
		DistHar.setFont(new Font("Dialog", Font.BOLD, 12));
		DistHar.setBounds(69, 201, 278, 23);
		
		
		JButton iniciar = new JButton("Iniciar");
		iniciar.setBounds(144, 364, 82, 36);
		
		painelPrincipal.setLayout(null);
		painelPrincipal.add(labelPrincipal);
		painelPrincipal.add(PotFund);
		painelPrincipal.add(DistHar);;
		painelPrincipal.add(iniciar);
	
		iniciar.setActionCommand("iniciar");
		
		ButtonGroup grupo1 = new ButtonGroup();
		
		grupo1.add(PotFund);
		grupo1.add(DistHar);
		
		jancont = new JanelaPrimariaCont(iniciar, PotFund, DistHar, framePrincipal);
		iniciar.addActionListener(jancont);
		
		framePrincipal.getContentPane().setLayout(null);
		framePrincipal.getContentPane().add(painelPrincipal);
		framePrincipal.setVisible(true);
	}
	

}