package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.InputMismatchException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;

import model.FormaOndaUc3;
import view.DesenhaGrafico;
import view.JanelaPrimariaView;

public class DistorcaoHarmonicaCont implements ActionListener{
	

	private JLabel serFour;
	private JButton okSeletores;
	private JFormattedTextField angFund;
	private JSpinner numHar;
	private JSpinner ordHar1;
	private JSpinner ordHar2;
	private JSpinner ordHar3;
	private JSpinner ordHar4;
	private JSpinner ordHar5;
	private JSpinner ordHar6;
	private JFormattedTextField angHar1;
	private JFormattedTextField angHar2;
	private JFormattedTextField angHar3;
	private JFormattedTextField angHar4;
	private JFormattedTextField angHar5;
	private JFormattedTextField angHar6;
	private JFormattedTextField ampHar1;
	private JFormattedTextField ampHar2;
	private JFormattedTextField ampHar3;
	private JFormattedTextField ampHar4;
	private JFormattedTextField ampHar5;
	private JFormattedTextField ampHar6;
	private JFormattedTextField ampFund;
	private JButton okButtonGraf1;
	private JButton okButtonGraf2;
	private JButton okButtonGraf3;
	private JButton okButtonGraf4;
	private JButton okButtonGraf5;
	private JButton okButtonGraf6;
	public DesenhaGrafico grafOndFund;
	public DesenhaGrafico grafOndRes;
	public DesenhaGrafico grafOrdHar1;
	public DesenhaGrafico grafOrdHar2;
	public DesenhaGrafico grafOrdHar3;
	public DesenhaGrafico grafOrdHar4;
	public DesenhaGrafico grafOrdHar5;
	public DesenhaGrafico grafOrdHar6;
	public FormaOndaUc3 calculos;
	public IniciaGraficosCont iniciador;
	
	public JanelaPrimariaView novasJanela;
	private JFrame framePassante;
	
	double nangFund;
	double nampFund;
	double nangHar1;
	double nampHar1;
	int nordHar1;
	double nangHar2;
	double nampHar2;
	int nordHar2;
	double nangHar3;
	double nampHar3;
	int nordHar3;
	double nangHar4;
	double nampHar4;
	int nordHar4;
	double nangHar5;
	double nampHar5;
	int nordHar5;
	double nangHar6;
	double nampHar6;
	int nordHar6;
	int valueNumHar = 0;
	String stringFinale = null;
	String stringAdicionale = null;

	
	public void passaGraficosHar(DesenhaGrafico grafOrdHar1, DesenhaGrafico grafOrdHar2, DesenhaGrafico grafOrdHar3, DesenhaGrafico grafOrdHar4, DesenhaGrafico grafOrdHar5, DesenhaGrafico grafOrdHar6) {
		this.grafOrdHar1 = grafOrdHar1;
		this.grafOrdHar2 = grafOrdHar2;
		this.grafOrdHar3 = grafOrdHar3;
		this.grafOrdHar4 = grafOrdHar4;
		this.grafOrdHar5 = grafOrdHar5;
		this.grafOrdHar6 = grafOrdHar6;
	}
	
	public void passaButtons(JButton okButtonGraf1, JButton okButtonGraf2, JButton okButtonGraf3, JButton okButtonGraf4, JButton okButtonGraf5, JButton okButtonGraf6) {
		this.okButtonGraf1 = okButtonGraf1;
		this.okButtonGraf2 = okButtonGraf2;
		this.okButtonGraf3 = okButtonGraf3;
		this.okButtonGraf4 = okButtonGraf4;
		this.okButtonGraf5 = okButtonGraf5;
		this.okButtonGraf6 = okButtonGraf6;
	}
	
	public void passaSpinnerOrd(JSpinner ordHar1, JSpinner ordHar2, JSpinner ordHar3, JSpinner ordHar4, JSpinner ordHar5, JSpinner ordHar6) {
		this.ordHar1 = ordHar1;
		this.ordHar2 = ordHar2;
		this.ordHar3 = ordHar3;
		this.ordHar4 = ordHar4;
		this.ordHar5 = ordHar5;
		this.ordHar6 = ordHar6;
		nordHar1 = (int) ordHar1.getValue();
		nordHar2 = (int) ordHar2.getValue();
		nordHar3 = (int) ordHar3.getValue();
		nordHar4 = (int) ordHar4.getValue();
		nordHar5 = (int) ordHar5.getValue();
		nordHar6 = (int) ordHar6.getValue();
	}
	
	public void passaAngulosHar(JFormattedTextField angHar1, JFormattedTextField angHar2, JFormattedTextField angHar3, JFormattedTextField angHar4, JFormattedTextField angHar5, JFormattedTextField angHar6) {
		this.angHar1 = angHar1;
		this.angHar2 = angHar2;
		this.angHar3 = angHar3;
		this.angHar4 = angHar4;
		this.angHar5 = angHar5;
		this.angHar6 = angHar6;
		nangHar1 =  Double.parseDouble(angHar1.getText());
		nangHar2 =  Double.parseDouble(angHar2.getText());
		nangHar3 =  Double.parseDouble(angHar3.getText());
		nangHar4 =  Double.parseDouble(angHar4.getText());
		nangHar5 =  Double.parseDouble(angHar5.getText());
		nangHar6 =  Double.parseDouble(angHar6.getText());
	}
	
	public void passaTextField(JFormattedTextField ampHar1, JFormattedTextField ampHar2, JFormattedTextField ampHar3, JFormattedTextField ampHar4, JFormattedTextField ampHar5, JFormattedTextField ampHar6) {
		this.ampHar1 = ampHar1;
		this.ampHar2 = ampHar2;
		this.ampHar3 = ampHar3;
		this.ampHar4 = ampHar4;
		this.ampHar5 = ampHar5;
		this.ampHar6 = ampHar6;
		nampHar1 = Double.parseDouble(ampHar1.getText());
		nampHar2 = Double.parseDouble(ampHar2.getText());
		nampHar3 = Double.parseDouble(ampHar3.getText());
		nampHar4 = Double.parseDouble(ampHar4.getText());
		nampHar5 = Double.parseDouble(ampHar5.getText());
		nampHar6 = Double.parseDouble(ampHar6.getText());
	}
	
	public void passaLabelFour(DesenhaGrafico grafOndFund, DesenhaGrafico grafOndRes,  JLabel serFour, JButton okSeletores, JSpinner numHar, JFormattedTextField angFund, JFormattedTextField ampFund) {
		this.grafOndFund = grafOndFund;
		this.grafOndRes = grafOndRes;
		this.serFour = serFour;
		this.okSeletores = okSeletores;
		this.numHar = numHar;
		this.angFund = angFund;
		this.ampFund = ampFund;

	}
	
	public void passaFrame(JFrame framePassante) {
		this.framePassante = framePassante;
	}
	

	public void escreveSerie(int numeroHar, String stringFinal, String stringAdicional) {
		if(numeroHar == 1) {
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)";  
			serFour.setText(stringAdicional);
		}
		else if(numeroHar == 2) {
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)"; 
			stringAdicional = stringAdicional + " + " + ampHar2.getText() + "cos( " +  ordHar2.getValue().toString() + "\u03C9t +" + angHar2.getText() + "°)"; 
			serFour.setText(stringAdicional);
		}
		else if(numeroHar == 3){
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar2.getText() + "cos( " +  ordHar2.getValue().toString() + "\u03C9t +" + angHar2.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar3.getText() + "cos( " +  ordHar3.getValue().toString() + "\u03C9t +" + angHar3.getText() + "°)"; 
			serFour.setText(stringAdicional);
		}
		else if(numeroHar == 4){
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar2.getText() + "cos( " + ordHar2.getValue().toString() + "\u03C9t +" + angHar2.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar3.getText() + "cos( " +  ordHar3.getValue().toString() + "\u03C9t +" + angHar3.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar4.getText() + "cos( " +  ordHar4.getValue().toString() + "\u03C9t +" + angHar4.getText() + "°)"; 
			serFour.setText(stringAdicional);
		}
		else if(numeroHar == 5){
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar2.getText() + "cos( " +  ordHar2.getValue().toString() + "\u03C9t +" + angHar2.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar3.getText() + "cos( " +  ordHar3.getValue().toString() + "\u03C9t +" + angHar3.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar4.getText() + "cos( " +  ordHar4.getValue().toString() + "\u03C9t +" + angHar4.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar5.getText() + "cos( " +  ordHar5.getValue().toString() + "\u03C9t +" + angHar5.getText() + "°)"; 
			serFour.setText(stringAdicional);
		}
		else if(numeroHar == 6){
			stringAdicional = stringFinal + " + " + ampHar1.getText() + "cos( " + ordHar1.getValue().toString() + "\u03C9t +" + angHar1.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar2.getText() + "cos( " + ordHar2.getValue().toString() + "\u03C9t +" + angHar2.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar3.getText() + "cos( " + ordHar3.getValue().toString() + "\u03C9t +" + angHar3.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar4.getText() + "cos( " + ordHar4.getValue().toString() + "\u03C9t +" + angHar4.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar5.getText() + "cos( " + ordHar5.getValue().toString() + "\u03C9t +" + angHar5.getText() + "°)"; 
			stringAdicional = stringAdicional +  " + " + ampHar6.getText() + "cos( " + ordHar6.getValue().toString() + "\u03C9t +" + angHar6.getText() + "°)"; 
			serFour.setText(stringAdicional);
		}
		
	}
	
	public void actionPerformed (ActionEvent evento) {
		String comando = evento.getActionCommand();
		calculos = new FormaOndaUc3();
		
		if(comando.equals("Voltar")) {
			framePassante.dispose();
			novasJanela = new JanelaPrimariaView();
			novasJanela.mostraInitPan();
		}
		try {						
			if(comando.equals("Seletor")) {
				nangFund = Double.parseDouble(angFund.getText());
				nampFund = Double.parseDouble(ampFund.getText());				
				if(ampFund.getText().matches("[-+]?\\d*\\.?\\d+")) {
					if(angFund.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(nampFund >= 0 && nampFund <= 220) {
							if(nangFund >= -180 && nangFund <= 180) {
								if((int) numHar.getValue() >= 0 && (int )numHar.getValue() <= 6) {	
										
										valueNumHar = (int) numHar.getValue();
										iniciador = new IniciaGraficosCont();
										grafOndFund.setScores(calculos.getArrayList(nampFund , nangFund));
										grafOrdHar1.setScores(iniciador.criaListHar());
										grafOrdHar2.setScores(iniciador.criaListHar());
										grafOrdHar3.setScores(iniciador.criaListHar());
										grafOrdHar4.setScores(iniciador.criaListHar());
										grafOrdHar5.setScores(iniciador.criaListHar());
										grafOrdHar6.setScores(iniciador.criaListHar());
										grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));
										stringFinale = "f(t) = " + ampFund.getText() + "cos( \u03C9t +" + angFund.getText() + "°)" ;
										
										if(valueNumHar == 0) {
											okButtonGraf1.setVisible(false);
											okButtonGraf2.setVisible(false);
											okButtonGraf3.setVisible(false);
											okButtonGraf4.setVisible(false);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
											stringAdicionale = "f(t) = " + ampFund.getText() + "cos( \u03C9t +" + angFund.getText() + "°)" ;
										}
										else if( valueNumHar == 1) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(false);
											okButtonGraf3.setVisible(false);
											okButtonGraf4.setVisible(false);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else if( valueNumHar == 2) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(true);
											okButtonGraf3.setVisible(false);
											okButtonGraf4.setVisible(false);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else if( valueNumHar == 3) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(true);
											okButtonGraf3.setVisible(true);
											okButtonGraf4.setVisible(false);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else if( valueNumHar == 4) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(true);
											okButtonGraf3.setVisible(true);
											okButtonGraf4.setVisible(true);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else if( valueNumHar == 5) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(true);
											okButtonGraf3.setVisible(true);
											okButtonGraf4.setVisible(true);
											okButtonGraf5.setVisible(true);
											okButtonGraf6.setVisible(false);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else if( valueNumHar == 6) {
											okButtonGraf1.setVisible(true);
											okButtonGraf2.setVisible(true);
											okButtonGraf3.setVisible(true);
											okButtonGraf4.setVisible(true);
											okButtonGraf5.setVisible(true);
											okButtonGraf6.setVisible(true);
											stringAdicionale = stringFinale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
											stringAdicionale = stringAdicionale + " + 0cos(0\u03C9t + 0°)" ;
										}
										else {
											okButtonGraf1.setVisible(false);
											okButtonGraf2.setVisible(false);
											okButtonGraf3.setVisible(false);
											okButtonGraf4.setVisible(false);
											okButtonGraf5.setVisible(false);
											okButtonGraf6.setVisible(false);
										}
									serFour.setText(stringAdicionale);
									
								}							
								else {	
									throw new IndexOutOfBoundsException("Graficos");
								}	
							}
							else {
								throw new IndexOutOfBoundsException("Angulo");
							}
						}
						else {
							throw new IndexOutOfBoundsException("Amplitude");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}
				}
				else {
					throw new InputMismatchException("Entrada Invalida");
				}
			}	
		
					
									


											
					
					
			else if(comando.equals("Sim1")){
				
				nampHar1 = Double.parseDouble(ampHar1.getText());
				nangHar1 =  Double.parseDouble(angHar1.getText());
				nordHar1 = (int) ordHar1.getValue();
					if(ampHar1.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar1.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar1 >= 0 && nampHar1 <= 220) {
								if(nangHar1 >= -180 && nangHar1 <= 180) {
									if((int) ordHar1.getValue() >= 0 && (int) ordHar1.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar1.setScores(calculos.getHarmonicoList(nampHar1, nangHar1, nordHar1));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}
			
			else if(comando.equals("Sim2")){
				
				nampHar2 = Double.parseDouble(ampHar2.getText());
				nangHar2 =  Double.parseDouble(angHar2.getText());
				nordHar2 = (int) ordHar2.getValue();
					if(ampHar2.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar2.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar2 >= 0 && nampHar2 <= 220) {
								if(nangHar2 >= -180 && nangHar2 <= 180) {
									if((int) ordHar2.getValue() >= 0 && (int) ordHar2.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar2.setScores(calculos.getHarmonicoList(nampHar2, nangHar2, nordHar2));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}
			
			else if(comando.equals("Sim3")){
				
				nampHar3 = Double.parseDouble(ampHar3.getText());
				nangHar3 =  Double.parseDouble(angHar3.getText());
				nordHar3 = (int) ordHar3.getValue();
					if(ampHar3.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar3.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar3 >= 0 && nampHar3 <= 220) {
								if(nangHar3 >= -180 && nangHar3 <= 180) {
									if((int) ordHar3.getValue() >= 0 && (int) ordHar3.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar3.setScores(calculos.getHarmonicoList(nampHar3, nangHar3, nordHar3));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}
			
			else if(comando.equals("Sim4")){
				
				nampHar4 = Double.parseDouble(ampHar4.getText());
				nangHar4 =  Double.parseDouble(angHar4.getText());
				nordHar4 = (int) ordHar4.getValue();
					if(ampHar4.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar4.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar4 >= 0 && nampHar4 <= 220) {
								if(nangHar4 >= -180 && nangHar4 <= 180) {
									if((int) ordHar4.getValue() >= 0 && (int) ordHar4.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar4.setScores(calculos.getHarmonicoList(nampHar4, nangHar4, nordHar4));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}
			else if(comando.equals("Sim5")){
				
				nampHar5 = Double.parseDouble(ampHar5.getText());
				nangHar5 =  Double.parseDouble(angHar5.getText());
				nordHar5 = (int) ordHar5.getValue();
					if(ampHar5.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar5.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar5 >= 0 && nampHar5 <= 220) {
								if(nangHar5 >= -180 && nangHar5 <= 180) {
									if((int) ordHar5.getValue() >= 0 && (int) ordHar5.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar5.setScores(calculos.getHarmonicoList(nampHar5, nangHar5, nordHar5));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}
			else if(comando.equals("Sim3")){
				
				nampHar6 = Double.parseDouble(ampHar6.getText());
				nangHar6 =  Double.parseDouble(angHar6.getText());
				nordHar6 = (int) ordHar6.getValue();
					if(ampHar6.getText().matches("[-+]?\\d*\\.?\\d+")) {
						if(angHar6.getText().matches("[-+]?\\d*\\.?\\d+")) {
							if(nampHar6 >= 0 && nampHar6 <= 220) {
								if(nangHar6 >= -180 && nangHar6 <= 180) {
									if((int) ordHar6.getValue() >= 0 && (int) ordHar6.getValue() <= 15) {	
									this.escreveSerie(valueNumHar, stringFinale, stringAdicionale);		
									grafOrdHar6.setScores(calculos.getHarmonicoList(nampHar6, nangHar6, nordHar6));
									grafOndRes.setScores(calculos.getFourrierSeries(nangFund, nampFund, nangHar1, nampHar1, nordHar1, nangHar2, nampHar2, nordHar2, nangHar3, nampHar3, nordHar3, nangHar4, nampHar4, nordHar4, nangHar5, nampHar5, nordHar5, nangHar6, nampHar6, nordHar6));	
									}							
									else {	
										throw new IndexOutOfBoundsException("Harmonicos");
									}	
								}
								else {
									throw new IndexOutOfBoundsException("Angulo");
								}
							}
							else {
								throw new IndexOutOfBoundsException("Amplitude");
							}
						}
						else {
							throw new InputMismatchException("Entrada Invalida");
						}
					}
					else {
						throw new InputMismatchException("Entrada Invalida");
					}	
			}			
		
		}
		catch(InputMismatchException mismatch) {
			JOptionPane.showMessageDialog(null, "Entrada Inválida.\nSó devem ser inseridos números!");
		}
		catch(IndexOutOfBoundsException outOfBounds) {
			if(outOfBounds.getMessage().equals("Amplitude")) {
				JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n0 <= Amplitude de Tensao <= 220");
			}
			else if(outOfBounds.getMessage().equals("Angulo")) {
				JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n-180 <= Ângulo de Fase <= 180");
			}
			else if(outOfBounds.getMessage().equals("Graficos")){
				JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n0 <= Número de Harmônicos <= 6");
			}
			else if(outOfBounds.getMessage().equals("Harmonicos")){
				JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n0 <= Ordem Harmônica <= 15");
			}
		}
		
	}

}

/*
 * 

								
 * 
 * 
 */
