package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.InputMismatchException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import model.FormaOndaUc2;
import view.DesenhaGrafico;
import view.DesenhaTriangulo;
import view.JanelaPrimariaView;


public class PotenciaFundamentalCont implements ActionListener{
	
	private JButton okTensao;
	JFormattedTextField anguloTensao;
	JFormattedTextField amplitudeTensao;
	public DesenhaGrafico graficoTensao;
	
	private JButton okCorrente;
	JFormattedTextField anguloCorrente;
	JFormattedTextField amplitudeCorrente;
	public DesenhaGrafico graficoCorrente;
	
	public DesenhaGrafico graficoPotencia;
	public DesenhaTriangulo triangulo;

	public JLabel potenciaAtiva;
	public JLabel potenciaReativa;
	public JLabel potenciaAparente;
	public JLabel fatorDePotencia;
	public JanelaPrimariaView novaJanela;
	private JFrame framePassante;
	
	private FormaOndaUc2 passavalor;
	Double angulo;
	Double amplitude;
	
	
	public void CalculosTensao (JButton okTensao, JFormattedTextField anguloTensao, JFormattedTextField amplitudeTensao, DesenhaGrafico graficoTensao) {
		this.okTensao = okTensao;
		this.anguloTensao = anguloTensao;
		this.amplitudeTensao = amplitudeTensao;
		this.graficoTensao = graficoTensao;
	}
	
	public void CalculosCorrente (JButton okCorrente, JFormattedTextField anguloCorrente, JFormattedTextField amplitudeCorrente, DesenhaGrafico graficoCorrente){
		this.okCorrente = okCorrente;
		this.anguloCorrente = anguloCorrente;
		this.amplitudeCorrente = amplitudeCorrente;
		this.graficoCorrente = graficoCorrente;
	}
	
	public void CalculosGerais(DesenhaGrafico graficoPotencia , DesenhaTriangulo triangulo , JLabel potenciaAtiva, JLabel potenciaReativa,  JLabel potenciaAparente, JLabel fatorDePotencia) {
		this.graficoPotencia = graficoPotencia;
		this.triangulo = triangulo;
		this.potenciaAtiva = potenciaAtiva;
		this.potenciaReativa = potenciaReativa;
		this.potenciaAparente = potenciaAparente;
		this.fatorDePotencia = fatorDePotencia;
		
		
	}
	
	public void PassaFrame(JFrame framePassante) {
		this.framePassante = framePassante;
	}
	
	/*
	 * anguloTensao.getText().matches("[0-9]+")
	 * JOptionPane.showMessageDialog(null, passavalor.PotenciaAtiva(amplitude, angulo));
			
	*/
	public void actionPerformed (ActionEvent evento) {
		String comando = evento.getActionCommand();
		passavalor = new FormaOndaUc2();
		
		if(comando.equals("Voltar")) {
			framePassante.dispose();
			novaJanela = new JanelaPrimariaView();
			novaJanela.mostraInitPan();
		}
		

		else {
			try {
				if(amplitudeTensao.getText().matches("[-+]?\\d*\\.?\\d+") && anguloTensao.getText().matches("[-+]?\\d*\\.?\\d+") && amplitudeCorrente.getText().matches("[-+]?\\d*\\.?\\d+") && anguloCorrente.getText().matches("[-+]?\\d*\\.?\\d+")){
					if(Double.parseDouble(amplitudeTensao.getText()) <= 220 && Double.parseDouble(amplitudeTensao.getText()) >= 0 ) {
						if(Double.parseDouble(amplitudeCorrente.getText()) <= 100 && Double.parseDouble(amplitudeCorrente.getText()) >= 0) {
							if(Double.parseDouble(anguloTensao.getText()) >= -180 && Double.parseDouble(anguloTensao.getText()) <= 180 && Double.parseDouble(anguloCorrente.getText()) >= -180 && Double.parseDouble(anguloTensao.getText()) <= 180) {
									double ampTensao =  Double.parseDouble(amplitudeTensao.getText());
									double angTensao =  Double.parseDouble(anguloTensao.getText());
									double ampCorrente = Double.parseDouble(amplitudeCorrente.getText());
									double angCorrente =  Double.parseDouble(anguloCorrente.getText());
							
									if(comando.equals("Tensao") ){
										graficoTensao.setScores(passavalor.getArrayList(ampTensao, angTensao));
										graficoPotencia.setScores(passavalor.getPotencia(ampTensao, angTensao, ampCorrente, angCorrente));
										potenciaAtiva.setText(String.valueOf(passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente)));
										potenciaReativa.setText(String.valueOf(passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente)));
										potenciaAparente.setText(String.valueOf(passavalor.getPotenciaAparente(passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente),passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente))));
										fatorDePotencia.setText(String.valueOf(passavalor.getFatorDePotencia(angTensao, angCorrente)));
										int passaAtiva = (int) passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente);
										int passaReativa = (int) passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente);
										triangulo.setPoints(passaAtiva, passaReativa);
									}
						
									else if(comando.equals("Corrente")){
										graficoCorrente.setScores(passavalor.getArrayList(ampCorrente, angCorrente));
										graficoPotencia.setScores(passavalor.getPotencia(ampTensao, angTensao, ampCorrente, angCorrente));
										potenciaAtiva.setText(String.valueOf(passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente)) + " W");
										potenciaReativa.setText(String.valueOf(passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente)) + " VAR" );
										potenciaAparente.setText(String.valueOf(passavalor.getPotenciaAparente(passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente),passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente)))+ " W");
										fatorDePotencia.setText(String.valueOf(passavalor.getFatorDePotencia(angTensao, angCorrente)));
										int passaAtiva = (int) passavalor.getPotenciaAtiva(ampTensao, angTensao, ampCorrente, angCorrente);
										int passaReativa = (int) passavalor.getPotenciaReativa(ampTensao, angTensao, ampCorrente, angCorrente);
										triangulo.setPoints(passaAtiva, passaReativa);
									}
							}
							
							
							else {
								throw new IndexOutOfBoundsException("Angulo");
							}
						}
						else {
							throw new IndexOutOfBoundsException("Corrente");
						}
					}
					else {
						throw new IndexOutOfBoundsException("Tensao");
					}
				}
				else {
					throw new InputMismatchException("Entrada Invalida");
				}
			}
			catch(InputMismatchException mismatch) {
			JOptionPane.showMessageDialog(null, "Entrada Inválida.\nSó devem ser inseridos números!");
			}
			catch(IndexOutOfBoundsException outOfBounds) {
				
				if(outOfBounds.getMessage().equals("Tensao")) {
					JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n0 <= Amplitude de Tensao <= 220");
				}
				else if(outOfBounds.getMessage().equals("Corrente")){
					JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n0 <= Amplitude de Corrente <= 100");
				}
				else if(outOfBounds.getMessage().equals("Angulo")) {
					JOptionPane.showMessageDialog(null, "Valor de Entrada Inválido.\n-180 <= Ângulo de Fase <= 180");
				}
			}
		
			
		}


	
	}
}



