package controller;

import java.util.ArrayList;

import model.FormaOndaUc2;
import model.FormaOndaUc3;

public class IniciaGraficosCont {
	
	FormaOndaUc3 passador = new FormaOndaUc3();
	
	public ArrayList<Double> criaList(){
		ArrayList<Double> devolve = new ArrayList<>();
		devolve = passador.getArrayList( 0 , 0 );
		return devolve;
	}
	
	public ArrayList<Double> criaListHar(){
		ArrayList<Double> devolve = new ArrayList<>();
		devolve = passador.getHarmonicoList( 0, 0, 0);
		return devolve;
	}
	
	

}
