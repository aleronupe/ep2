package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import view.DistorcaoHarmonicaView;
import view.PotenciaFundamentalView;


public class JanelaPrimariaCont implements ActionListener{
		private JButton buttonpassante;
		private JRadioButton radio1;
		private JRadioButton radio2;
		private JFrame framepassante;
		public PotenciaFundamentalView potFundView;
		public DistorcaoHarmonicaView distHarView;

		
		public JanelaPrimariaCont (JButton buttonpassante, JRadioButton radio1, JRadioButton radio2, JFrame framepassante ) {
			this.buttonpassante = buttonpassante;
			this.radio1 = radio1;
			this.radio2 = radio2;
			this.framepassante = framepassante;
		}
		
		public void actionPerformed (ActionEvent evento) {
			if(evento.getSource() == buttonpassante && radio1.isSelected()){
				framepassante.dispose();
				potFundView = new PotenciaFundamentalView();
				potFundView.mostraPotFund();
			}
			else if(evento.getSource() == buttonpassante && radio2.isSelected()){
				framepassante.dispose();
				distHarView = new DistorcaoHarmonicaView();
				distHarView.mostraDistHar();
			}
		}
		


		
}		
	


