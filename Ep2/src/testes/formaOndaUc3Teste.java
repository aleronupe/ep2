package testes;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.FormaOndaUc3;

public class formaOndaUc3Teste {
	FormaOndaUc3 testaOndaUc3;
	ArrayList<Double> testaArray;
	ArrayList<Double> testaHarmonicoArray;
	@Before
	public void setUp() throws Exception {
		testaOndaUc3 = new FormaOndaUc3();
		testaHarmonicoArray = new ArrayList<Double>();
		testaArray = new ArrayList<Double>();
		double value = 0.0;
		double cont = 0.0;
		for(cont = 0.0; cont < 100; cont++) {
			value = 0.0;
			value = 22*Math.cos((2*3.14*60*cont)+ Math.toRadians(-180));
			value += 34*Math.cos((2*2*3.14*60*cont)+ Math.toRadians(-90));
			value += 46*Math.cos((4*2*3.14*60*cont)+ Math.toRadians(-30));
			value += 58*Math.cos((6*2*3.14*60*cont)+ Math.toRadians(0));
			value += 70*Math.cos((8*2*3.14*60*cont)+ Math.toRadians(30));
			value += 82*Math.cos((10*2*3.14*60*cont)+ Math.toRadians(90));
			value += 94*Math.cos((14*2*3.14*60*cont)+ Math.toRadians(180));
			testaArray.add(value);
		}
		
		for(cont = 0.0; cont < 100; cont++) {
			value = 34*Math.cos((0*2*3.14*60*cont)+ Math.toRadians(96));
			testaHarmonicoArray.add(value);
		}
		
	}

	@Test
	public void testGetFourrierSeries() {
		assertEquals(testaArray, testaOndaUc3.getFourrierSeries(-180.0 , 22.0 , -90.0 , 34.0, 2, -30.0, 46.0, 4, 0.0, 58.0, 6, 30.0, 70.0, 8, 90.0, 82.0, 10, 180.0 , 94.0, 14));
	}

	@Test
	public void testGetHarmonicoList() {
		assertEquals(testaHarmonicoArray, testaOndaUc3.getHarmonicoList(34, 96, 0));
	}

}
