package testes;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.FormaOndaUc2;

public class formaOndaUc2Teste {
	
	FormaOndaUc2 testaUc2;
	ArrayList<Double> testaArray;
	ArrayList<Double> testaArrayPotencia;
	@Before
	public void setUp() throws Exception {
		testaUc2 = new FormaOndaUc2();
		
		testaArray = new ArrayList<Double>();
		double result = 0.0;
		double cont = 0.0;
		for(cont = 0.0; cont < 100; cont++) {
			result = 100*Math.cos((2*3.14*60*cont)+Math.toRadians(85));
			testaArray.add(result);
		}
		
		testaArrayPotencia = new ArrayList<Double>();
		result = 0.0;
		double tensao = 0.0;
		double corrente = 0.0;
		for(cont = 0.0; cont < 100; cont += 0.1) {
			tensao = 100*Math.cos((2*3.14*60*cont)+Math.toRadians(180));
			corrente = 80*Math.cos((2*3.14*60*cont)+Math.toRadians(210));
			result = Math.abs(tensao)*Math.abs(corrente)*Math.sin(Math.toRadians(30));
			testaArrayPotencia.add(result);
		}

	}

	@Test
	public void testGetArrayList() {
		assertEquals(testaArray, testaUc2.getArrayList(100, 85));
	}

	@Test
	public void testGetPotencia() {
		assertEquals(testaArrayPotencia, testaUc2.getPotencia(100, -180, 80, -150));
	}

	@Test
	public void testGetPotenciaAtiva() {
		assertEquals(7028, testaUc2.getPotenciaAtiva(220, 0, 39, 35), 1);
	}

	@Test
	public void testGetPotenciaReativa() {
		assertEquals(-4921, testaUc2.getPotenciaReativa(220, 0, 39, 35), 1);
	}

	@Test
	public void testGetFatorDePotencia() {
		assertEquals(0.82, testaUc2.getFatorDePotencia(0, 35), 0.01);
	}

	@Test
	public void testGetPotenciaAparente() {
		assertEquals(8579, testaUc2.getPotenciaAparente(testaUc2.getPotenciaReativa(220, 0, 39, 35),testaUc2.getPotenciaAtiva(220, 0, 39, 35)), 1);
	}

}
